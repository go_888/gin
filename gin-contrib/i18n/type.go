package i18n

import (
	"gitee.com/go_888/gin"
)

type (
	// GetLngHandler ...
	GetLngHandler = func(context *gin类.Context, defaultLng string) string

	// Option ...
	Option func(GinI18n)
)
