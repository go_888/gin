package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
	
	"gitee.com/go_888/gin/gin-contrib/gzip"
	"gitee.com/go_888/gin"
)

func main() {
	r := gin类.X创建默认对象()
	r.X中间件(gzip.Gzip(gzip.DefaultCompression))
	r.X绑定GET("/ping", func(c *gin类.Context) {
		c.X输出文本(http.StatusOK, "pong "+fmt.Sprint(time.Now().Unix()))
	})

	// 在0.0.0.0:8080监听并服务
	if err := r.X监听(":8080"); err != nil {
		log.Fatal(err)
	}
}
